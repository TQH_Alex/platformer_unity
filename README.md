# README #

This README would normally document whatever steps are necessary to get your application up and running.

# Astron the robot is here to save the day and shoot some aliens and maybe monsters #

### What is this repository for? ###

* The project is a platform game i build when i was attended the Game Development course at TDT University
* The game has been port to Android from PC, but the main platform for the game is still PC
* Some picture for action : 
* Our Hero : ![Hero.png](https://bitbucket.org/repo/qdBA5R/images/2159417439-Hero.png)

### How do I get set up? ###

* Install Unity (at least version 5.4 and above).
* A code editor : Visual Studio, Mono ...
* All dependencies should have came with the source code, but if anything missing, or you want to check them out individually then there are two of them : [A* Pathfinding Project](https://arongranberg.com/astar/), and [Standard Assets for Unity 4.6
](https://www.assetstore.unity3d.com/en/#!/content/21064).
* Anybody that interested in this little project should also check out [How to make a 2D Platformer - Unity Course](https://www.youtube.com/playlist?list=PLPV2KyIb3jR42oVBU6K2DIL6Y22Ry9J1c). I have learn so much from just wactching these video.

### Notice ###

* if the project have an asset that i have not contributed to you, please let me know through the feedback.

### Who do I talk to? ###

* TQH

## MIT License ##

Copyright (c) [2017] [Truong Quoc Hung]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.