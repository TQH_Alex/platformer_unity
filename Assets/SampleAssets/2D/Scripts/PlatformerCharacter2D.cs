﻿using System.Collections;
using UnityEngine;

namespace UnitySampleAssets._2D
{

    public class PlatformerCharacter2D : MonoBehaviour
    {
        private bool facingRight = true; // kiem tra xem nguoi choi dang quay mat o huong nao

        [SerializeField] private float maxSpeed = 10f; // The fastest the player can travel in the x axis.
        [SerializeField] private float jumpForce = 400f; // Amount of force added when the player jumps.	

        [Range(0, 1)] [SerializeField] private float crouchSpeed = .36f;
                                                     // Amount of maxSpeed applied to crouching movement. 1 = 100%

        [SerializeField] private bool airControl = false; // Whether or not a player can steer while jumping;
        [SerializeField] private LayerMask whatIsGround; // A mask determining what is ground to the character

        private Transform groundCheck; // A position marking where to check if the player is grounded.
        private float groundedRadius = .5f; // Radius of the overlap circle to determine if grounded
        private bool grounded = false; // Whether or not the player is grounded.
        private Transform ceilingCheck; // A position marking where to check for ceilings
        private float ceilingRadius = .2f; // Radius of the overlap circle to determine if the player can stand up
        Animator anim; // Reference to the player's animator component.
        private Platformer2DUserControl controller;

        private void Awake()
        {
            // Setting up references.
            groundCheck = transform.Find("GroundCheck");
            ceilingCheck = transform.Find("CeilingCheck");
            anim = GetComponent<Animator>();
            controller = GetComponent<Platformer2DUserControl>();
        }


        private void FixedUpdate()
        {
            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            grounded = Physics2D.OverlapCircle(groundCheck.position, groundedRadius, whatIsGround);
            anim.SetBool("Ground", grounded);

            // Set the vertical animation
           anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);
        }


        public void Move(float move, bool crouch, bool jump,bool shoot, bool melee,bool slide, float slideSpeed)
        {

            // khi truot thi khong duoc ban va can chien
            if(anim.GetBool("Slide") )
            {
                if ( !facingRight)
                {
                    slideSpeed = -slideSpeed;
                }
                crouch = false;
                shoot = false;
                melee = false;
            }

            if(controller.GetKnockBack())
            {
                move = 0;

            }
            // If crouching, check to see if the character can stand up
            if (!crouch && anim.GetBool("Crouch"))
            {
                // If the character has a ceiling preventing them from standing up, keep them crouching
                if (Physics2D.OverlapCircle(ceilingCheck.position, ceilingRadius, whatIsGround))
                    crouch = true;
            }

            // Set whether or not the character is crouching in the animator
            anim.SetBool("Crouch", crouch);
            
            anim.SetBool("Shoot", shoot); //  ban sung
            // bien set danh can chien 
         
            if(melee && !anim.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Idel"))
            {
                anim.SetBool("Melee", true);
            
          
            }
         
            // bien set truot-slide 
            if ((slide && !anim.GetCurrentAnimatorStateInfo(0).IsName("Player_Slide"))&& Mathf.Abs(move) > 0.5)
            {
                anim.SetBool("Slide", true);    
            }
                else if(!grounded)
            {
                anim.SetBool("Slide", false);
            }
            // neu o tren mat dat, va danh can chien thi uu tien can chien         
            if(grounded && melee)
            {
                move = 0f;
            }
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Idel") || anim.GetNextAnimatorStateInfo(0).IsName("Melee_Jump"))
            {
                anim.speed = 1.3f;
            }
            else
            {// kiem soat toc do cua cac animation
                anim.speed = 1.1f;
            }
            //chi kiem soat nhan vat khi dang o mat dat hoac khi airControl duoc bat
            if (  ( grounded || airControl))
            {
                
                // Reduce the speed if crouching by the crouchSpeed multiplier
                move = (crouch ? move*crouchSpeed : move);

                // The Speed animator parameter is set to the absolute value of the horizontal input.
               

                    anim.SetFloat("Speed", Mathf.Abs(move));
                

                // Move the character
                // chi di chuyen khi khong truot
                if (anim.GetBool("Slide") )
                {

                    gameObject.GetComponent<BoxCollider2D>().enabled = false;

                    GetComponent<Rigidbody2D>().velocity = new Vector2(slideSpeed * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
                    if (move > 0 && !facingRight)
                    {
                        Flip();
                    }
                    // Otherwise if the input is moving the player left and the player is facing right...
                    else if (slideSpeed < 0 && facingRight)
                    {
                        Flip();
                    }


                }
                else
                {
                    

                    GetComponent<Rigidbody2D>().velocity = new Vector2(move * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

                    // If the input is moving the player right and the player is facing left...
                    if (move > 0 && !facingRight)
                        // ... flip the player.
                        Flip();
                    // Otherwise if the input is moving the player left and the player is facing right...
                    else if (move < 0 && facingRight)
                        // ... flip the player.
                        Flip();
                }
            }

            // If the player should jump...

            if (grounded && jump && !(Physics2D.OverlapCircle(ceilingCheck.position, ceilingRadius, whatIsGround)))
            {
                gameObject.GetComponent<BoxCollider2D>().enabled = enabled;
                grounded = false;
                anim.SetBool("Ground", false);
               // GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));
                // Add a vertical force to the player.
                GetComponent<Rigidbody2D>().velocity = new Vector3(GetComponent<Rigidbody2D>().velocity.x, 
                    jumpForce/55);
            } 
           
          
        }
        // khi truot ket thuc thi ta doi bien no lai
        private void OnSlideFinish ()
        {
            if (Physics2D.OverlapCircle(ceilingCheck.position, ceilingRadius, whatIsGround))
            {
               
                anim.SetBool("Slide", true);
                StartCoroutine(checkSlideCeil());
            }
            else
            {
                gameObject.GetComponent<BoxCollider2D>().enabled = true;
                anim.SetBool("Slide", false);
                return;
            }
        }
        // khi danh can chien ket thuc
        private void OnMeleeFinish()
        {
            anim.SetBool("Melee", false);
        }
        // Quay nhan qua trai hoac qua phai
        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            facingRight = !facingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
        // Day lui nguoi choi lai neu dap chung spike-dinh
        public IEnumerator KnockBackSpike(float knockDur, float knockbackPwr, Vector3 knockbackDir)
        {

            float timer = 0;

            while (knockDur > timer)
            {
               timer += Time.deltaTime;
              //  GetComponent<Rigidbody2D>().AddForce(new Vector3(knockbackDir.x * -100, knockbackDir.y * knockbackPwr, transform.position.z));
                GetComponent<Rigidbody2D>().velocity = (new Vector3(knockbackDir.x * -2, knockbackDir.y * knockbackPwr, transform.position.z));
            }

            yield return 0;

        }
        // Day luoi nguoi choi quai vat
        public void KnockBackMonster( float knockbackPwr,bool knockbackDir)
        {
            if (knockbackDir)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-knockbackPwr * 2, knockbackPwr * 2);
                // GetComponent<Rigidbody2D>().AddForce(-transform.forward * knockbackPwr);

            }
            else {
                GetComponent<Rigidbody2D>().velocity = new Vector2(knockbackPwr * 2, knockbackPwr * 2);
                // GetComponent<Rigidbody2D>().AddForce(transform.forward * knockbackPwr);
            
            }
        }
        IEnumerator checkSlideCeil ()
        {
            
            yield return new WaitForSeconds(0.55f);
            if (!(Physics2D.OverlapCircle(ceilingCheck.position, ceilingRadius, whatIsGround)) && anim.GetBool("Slide"))
            {
                anim.SetBool("Slide", false);
                gameObject.GetComponent<BoxCollider2D>().enabled = true;
            }
        }
    }
}