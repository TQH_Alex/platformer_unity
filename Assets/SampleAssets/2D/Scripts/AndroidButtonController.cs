﻿using System;
using UnityEngine;

public class AndroidButtonController : MonoBehaviour {
   
    void Start()
    {
        
    }
    private void FixedUpdate()
    {
        GameMaster.jumpAndroid = false;
        //  GameMaster.fireAndroid = false;
        if (GameMaster.slideAndroid)
        {
            GameMaster.slideAndroid = false;
        }

    }
    public void PressJumpButton ()
    {
        GameMaster.jumpAndroid = true;

    }
    public void PressFireButton()
    {
        GameMaster.fireAndroid = true;

    }
    public void PressSlideButton()
    {
        GameMaster.slideAndroid = true;

    }
     
    // Bo ngon tay ra khoi nut ban
    public void FreePressFireButton()
    {
        GameMaster.fireAndroid = false;
    }
    public void PressChangeWeaponButton()
    {
        GameMaster.changeWeapon = true;
    }
    // hien tai khong nhan nut nay
    public void FreePressChangeWeaponButton()
    {
        GameMaster.changeWeapon = false;
    }
}
