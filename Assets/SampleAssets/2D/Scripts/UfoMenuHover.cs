﻿using UnityEngine;
using System.Collections;

public class UfoMenuHover : MonoBehaviour {

    public Transform target;
    bool wait = true;
    Vector3 orgin;
    bool reverse = false;
   // public Transform ufo;
	// Use this for initialization
	void Start () {
        if(target == null)
        { }
        else {
            orgin = transform.position;   
        }

	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (wait)
        {
            StartCoroutine(WaitFor(15));
        }
        else
        {
            MoveUFO();
        }
	}
    void MoveUFO()
    {
        // neu khoang cach tu ufo den dich be hon 1 thi di nguoc lai
        if(Mathf.Abs(transform.position.x - target.position.x) < 2)
        {
            reverse = true;
        }
        if (Mathf.Abs(transform.position.x - orgin.x) < 2)
        {
            reverse = false;
            wait = true;
        }
            if (!reverse)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * 0.1f);

        }
        else
        {
           
            transform.position = Vector3.Lerp(transform.position, orgin, Time.deltaTime * 0.1f);
        }
    }
    IEnumerator WaitFor(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        wait = false;
    }
}
