﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {

    /// <summary>
    /// Android Version
    /// </summary>
    public static bool jumpAndroid;
    public static bool fireAndroid;
    public static bool slideAndroid;
    public static bool changeWeapon;

    // lop singleton
    public static GameMaster gm;
    // ten cua am thanh respawn
    public string respawnCountdownSoundName = "Respawn";

    // ten cua anm spawn khi chet
    public string spawnSoundName = "Spawn";

    // Ten cua am thanh khi GameOver
    public string gameOverSoundName = "GameOver";

    //Danh sach ke thu
    public GameObject EnemyList;
    public GameObject BossBattle;
    public int victoryPoint;
    static bool victory = false;
    static bool end ;

    [SerializeField]
    private GameObject gameGuidePC;
    // Bien quan ly nut cho Android
    [SerializeField]
    private GameObject androidButton;
    // Bien quan ly nut cho Android Joystick
    [SerializeField]
    private GameObject androidJoystick;

    // so mang cua nguoi choi
    [SerializeField]
    private int maxLives = 3;
    private static int _remainingLives = 3;
    public static int RemainingLives
    {
        get { return _remainingLives; }
    }



    public Transform playerPrefab;
    public Transform[] spawnPoint;
    public static int spawnCount;
    public float spawnDelay = 2; // thoi gian doi sau khi spawn
    public Transform spawnPrefab; // hieu ung khi hoi sinh

    //cache
    private AudioManager audioManager;

   


    public static void Nullify()
    {
        gm = null;
    }


    void Awake ()
    {
        // khoi tao so mang + diem + diem hoi sinh
        Player.freezingPlayer = false;
        gm = this;
        _remainingLives = maxLives;
        Player.PlayerStats.PlayerScore = 0;
        spawnCount = 0;
        EnemyList.SetActive(false);
        SpawnPointController.isPlaying = false;
        victory = false;
        end = false;
        BossBattle.SetActive(false);
        //

        //cai dau cau hinh cho android

    }
    void Start()
    {
        
        if (gm == null)
        {
            gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
        }
        //caching
        // khoi tao audio manager
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("FREAK OUT! No AudioManager found in the scene.");
        }
        // Hien la ban android
#if UNITY_ANDROID
        androidButton.SetActive(true);
        gameGuidePC.SetActive(false);
        androidJoystick.SetActive(true);
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;
        QualitySettings.antiAliasing = 4;
        QualitySettings.shadowCascades = 2;
        QualitySettings.shadowDistance = 70;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
#endif

    }



    [SerializeField]
    private GameObject gameOverUI;
    [SerializeField]
    private GameObject pauseMenu;
  


    public static bool keyEscape = false;
    bool pause = false;
    [SerializeField]
    private WaveSpawner waveSpawner;

    // dung de kich thoat man hinh Pause, se ngung tat ca ke thu lai
    public delegate void PauseMenuCallback(bool active);
    public PauseMenuCallback onTogglePauseMenu;




    [SerializeField]
    private GameObject gameVictory;
    // dung de kich thoat man hinh Victory, se ngung tat ca ke thu lai
    public delegate void VictoryMenuCallback(bool active);
    public VictoryMenuCallback onToggleVictoryMenu;
    void Update()
    {
        if (spawnCount == 1)
        {
            EnemyList.SetActive(true);
        }
        if (spawnCount == 2)
        {
            BossBattle.SetActive(true);
        }
        if (gameGuidePC != null && GameMaster.spawnCount != 0)
        {
            gameGuidePC.SetActive(false);
        }
        // kich thoat delegate cho victory menu
        if (spawnCount == victoryPoint && !victory)
        {
            ToggleVictoryMenu();
            EndGameVictory();
            victory = true;
        }

        if ((Input.GetKeyDown(KeyCode.Escape) || keyEscape) && ! victory)
        {
            if (onTogglePauseMenu == null)
            {
             
            }
            else
            {
                TogglePauseMenu();
                keyEscape = false;
                if (!pause)
                {
                    audioManager.PauseAllSound();
                    audioManager.PlaySound("Pause");
                    pause = true;
                }
                else
                {
                    audioManager.UnPauseAllSound();
                    audioManager.StopSound("Pause");
                    pause = false;
                }
            }
        }

        // Hien la ban android
#if UNITY_ANDROID
        // xet dieu kien cho luc nao nut Anrdoid hien len
        if (victory || end || pause )
        {
            androidButton.SetActive(false);
            //androidJoystick.SetActive(false);
            androidJoystick.GetComponent<CanvasGroup>().alpha = 0;
        }
        else {
            androidButton.SetActive(true);
            androidJoystick.GetComponent<CanvasGroup>().alpha = 0.6f;
            //androidJoystick.SetActive(true);
        }
#endif
    }
    // tat waveSpawner  va bat pause Menu
    void TogglePauseMenu()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        if (waveSpawner != null)
        {
            waveSpawner.enabled = !pauseMenu.activeSelf;
        }
        onTogglePauseMenu.Invoke(pauseMenu.activeSelf);
    }
    // man hinh Victory
    void ToggleVictoryMenu()
    {
        gameVictory.SetActive(!gameVictory.activeSelf);
        if (waveSpawner != null)
        {
            waveSpawner.enabled = !gameVictory.activeSelf;
        }
        onToggleVictoryMenu.Invoke(gameVictory.activeSelf);
    }

    // Kich hoat ket thuc man choi
    public void EndGame()
    {
        // choi nhac gameover khi chet
        audioManager.StopAllSound();
        audioManager.PlaySound("GameOver");
        Debug.Log("GAME OVER");
        gameOverUI.SetActive(true);
    }
    // Kich hoat ket thuc man choi
    public void EndGameVictory()
    {
        // choi nhac gameover khi chet
        audioManager.StopAllSound();
        audioManager.PlaySound("Victory");
        Debug.Log("Victory");
        end = true;
        EnemyList.SetActive(false);
        gameVictory.SetActive(true);
    }

    public IEnumerator _RespawnPlayer()
    {
        // am thanh dem nguoc truoc khi hoi sinh
        audioManager.PlaySound(respawnCountdownSoundName);
        yield return new WaitForSeconds(spawnDelay);
        // am thanh khi hoi sinh
        audioManager.PlaySound(spawnSoundName);
        Instantiate(playerPrefab, spawnPoint[spawnCount].position, spawnPoint[spawnCount].rotation);
       Transform clone = Instantiate(spawnPrefab, spawnPoint[spawnCount].position, spawnPoint[spawnCount].rotation) as Transform;
        Destroy(clone.gameObject, 3f);
    }
   
    // ham static co the duoc truy cap tu tat ca moi moi
    public static void KillPlayer(Player player)
    {
        Destroy(player.gameObject);
        _remainingLives -= 1;
        // neu nguoi choi chet, ket thuc man
        if (_remainingLives <= 0 && !victory)
        {
            
            gm.EndGame();
        }
        else
        {
            // vong lap coroutine
            gm.StartCoroutine(gm._RespawnPlayer());
        }
    }
    public static void KillEnemy(Enemy enemy)
    {
        Destroy(enemy.gameObject);
    }
    
}
