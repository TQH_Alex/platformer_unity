﻿using UnityEngine;
using System.Collections;

public class GloopController : MonoBehaviour {

    // toc do va huong di chuyen
    public float moveSpeed;
    bool moveLeft;

    public Transform wallCheck; // kiem tra dung tuong
    public float wallCheckRadi; // luc nao dung tuong
    public LayerMask whatIsWall; // vat gi la tuong
    private bool _hittingWall;
    private bool _headCheck;
    Transform groundCheck;
    Transform headCheck;
    Animator anim;

    public Transform edgeCheck;
    private bool _notHittingEdge;

	// Use this for initialization
	void Start () {
	
	}
    private void Awake()
    {
        headCheck = transform.Find("HeadCheck");
        groundCheck = transform.Find("GroundCheck");
        anim = GetComponent<Animator>();
    }

        // Update is called once per frame
        void FixedUpdate () {
        _hittingWall = Physics2D.OverlapCircle(wallCheck.position,wallCheckRadi,whatIsWall);
        // kiem tra dau
        if (headCheck != null)
        {
            _headCheck = Physics2D.OverlapCircle(headCheck.position, wallCheckRadi, whatIsWall);
        }
        _notHittingEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadi,whatIsWall);
        // neu dung tuong hoac cham canh 
        if(_hittingWall || !_notHittingEdge || _headCheck)
        {
            moveLeft = !moveLeft;
            Flip();
          
        }
        // neu di chuyen qua trai
        if(moveLeft)
        {
            
           GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
           transform.localEulerAngles  = new Vector3(0, 0, 0); // set roration
           anim.SetFloat("Speed", Mathf.Abs(moveSpeed));
          
        }
        else
        {
           //
            GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            transform.localEulerAngles = new Vector3(0, 0, 0); // set roration
            anim.SetFloat("Speed", Mathf.Abs(moveSpeed));
          // Flip();
        }
	
	}
    //Quay quai vat qua trai
    private void Flip()
    {
        // Quay gloop qua trai
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
    
        if (other.gameObject.tag == "Enemy")
        {
                moveLeft = !moveLeft;
                Flip();
            
        }
    }
    public bool getMoveLeft()
    {
        return moveLeft;
    }
    public void SetMoveDirection()
    {
        moveLeft = !moveLeft;
        Flip();
    }
    public void SetMoveSpeed(float movespeed)
    {
        moveSpeed = movespeed;
    }

}
