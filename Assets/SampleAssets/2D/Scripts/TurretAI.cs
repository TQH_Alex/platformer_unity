﻿using UnityEngine;
using System.Collections;
using System;

public class TurretAI : MonoBehaviour {

    // float 
    public float distance;
    public float wakeupRange;
    public float shootInteval;
    public float bulletSpeed = 100;
    public float timer;

    //boolean
    bool awake;
    public bool lookinkRight;
    private static bool wakeUpFinish = false;
    bool tooClose;
    bool inWakeUpRangeTooFar = true;

    //ref
    public GameObject bullet;
    public GameObject target;
    public Transform firePointLeft;
    public Transform firePointRight;
    public Animator anim;

    //cache
    private AudioManager audioManager;
    // chay truoc start
    void Awake() {
        anim = gameObject.GetComponent<Animator>();
    }
	// Use this for initialization
	void Start () {
        //caching
        // khoi tao audio manager
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("FREAK OUT! No AudioManager found in the scene.");
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }
        else
        {

            //awake = anim.GetBool("Awake");
            //lookinkRight = anim.GetBool("LookingRight");
            RangeCheck();
        }
      
	}

    private void RangeCheck()
    {
        distance = Vector3.Distance(transform.position, target.transform.position);
        float right = transform.position.x - target.transform.position.x;
        if(distance < wakeupRange)
        {
            awake = true;
           
            // right > 0 nhin ben phai khong thi nhin ben trai
            if (right < 0)
            {
                lookinkRight = true;
            }
            else { lookinkRight = false; }
          
        }
        if(distance > wakeupRange)
        {
            awake = false;
            
        }
        // qua xa nguoi choi nhung van dang trong tam nhin
        if ((Mathf.Abs(transform.position.x - target.transform.position.x) > 7 && awake == true))
         { 
            inWakeUpRangeTooFar = true;
        }
        if ((Mathf.Abs(transform.position.x - target.transform.position.x) < 7 && awake == true))
        {
            inWakeUpRangeTooFar = false;
        }
        if (Mathf.Abs(transform.position.x - target.transform.position.x) < 5 )
        {
            tooClose = true;
        }
        else
        {
            tooClose = false;
        }
        anim.SetBool("Awake", awake);
    
        anim.SetBool("LookingRight", lookinkRight);
    }
    // turret tan cong
    public void Attack (bool attackFromRight)
    {
       // print("Attack");
        timer += Time.deltaTime;
        if(timer >= shootInteval)
        {
            // tao huong ban dan
            Vector2 direction =  target.transform.position - transform.position;
            direction.Normalize();
            // ban tu ben trai
            if(!attackFromRight && wakeUpFinish)
            {
                GameObject bulletClone;
                bulletClone = Instantiate(bullet, firePointLeft.position, firePointLeft.rotation) as GameObject;
                bulletClone.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

                timer = 0;
            }
            // ben tu ben phai
            if(attackFromRight && wakeUpFinish)
            {
                GameObject bulletClone;
                bulletClone = Instantiate(bullet, firePointRight.position, firePointRight.rotation) as GameObject;
                bulletClone.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

                timer = 0;
            }
        }

    }
    private void turretWakeUp()
    {
        audioManager.PlaySound("Turret_Awake");
    }
    private void turretSleep()
    {
        // dung chung mot am thanh cho thuc va ngu
        audioManager.PlaySound("Turret_Awake");
        wakeUpFinish = false;
    }
    private void turretWakeUpFinish()
    {
        wakeUpFinish = true;
    }
    private void turretSee()
    {
        audioManager.PlaySound("Turret_See");
    }
    public bool GetAwake()
    {
        return awake;
    }
    public bool  GetTurretLookingRight()
    {
        return lookinkRight;
    }
    public bool GetTurretTooClose()
    {
      
        return tooClose;
    }
    public bool GetTurretTooFar()
    {

        return inWakeUpRangeTooFar;
    }
    public float GetDistance ()
    {
        return distance;
    }
    public float GetWakeUpRange()
    {
        return wakeupRange;
    }
}
