﻿using System.Collections;
using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;

namespace UnitySampleAssets._2D
{

    [RequireComponent(typeof(PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {

        /// <summary>
        /// Window version
        /// </summary>
        private PlatformerCharacter2D character;
        private Player player;
        private bool jump;
        //  private static int countJump = 0;
        //   private  bool doubleJump ; // nhay 2 lan
        bool knockBack; // bi danh lui lai

        // bien dieu khieu nut Android
        private AndroidButtonController androidController;
        private void Awake()
        {
            player = GetComponent<Player>();
            character = GetComponent<PlatformerCharacter2D>();
            androidController = GetComponent<AndroidButtonController>();

        }
        // truong hop cua window
#if UNITY_STANDALONE_WIN
        private void Update()
        {
         
            if (!jump)
            // Read the jump input in Update so button presses aren't missed.

            {

                jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
           
        }
       
        private void FixedUpdate()
        {
         
            if (!knockBack)
            {
                // Read the inputs.
                bool crouch = Input.GetKey(KeyCode.LeftControl);
                bool shoot = Input.GetButton("Fire1");
                //  bool melee = Input.GetButton("Fire2");
                bool melee = false;
                bool slide = Input.GetKey(KeyCode.LeftShift);
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float slideSpeed = 1.5f;
                // Pass all parameters to the character control script.

                character.Move(h, crouch, jump, shoot, melee, slide, slideSpeed);
            }
            else
            {
               
                StartCoroutine(Wait());

            }


            jump = false;
        }
#endif
#if UNITY_ANDROID
        private void Update()
        {
            if (!jump)
            // Read the jump input in Update so button presses aren't missed.

            {

                jump = GameMaster.jumpAndroid;
            }
        }
          private void FixedUpdate()
        {

            if (!knockBack)
            {
                bool crouch = Input.GetKey(KeyCode.LeftControl);
                bool shoot =   GameMaster.fireAndroid;
                bool melee = false;
                bool slide =  GameMaster.slideAndroid;
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float slideSpeed = 1.5f;
                // Pass all parameters to the character control script.

                character.Move(h, crouch, jump, shoot, melee, slide, slideSpeed);
            }
            else
            {
                StartCoroutine(Wait());

            }


            jump = false;
        }
#endif
        void OnCollisionEnter2D(Collision2D other)
        {
            /*
             *  
             */

            bool knockBackFromRight = false;
            if (character.transform.position.x < other.transform.position.x)
            {
                knockBackFromRight = true;
            }
            else { knockBackFromRight = false; }

            if (other.gameObject.tag == "Spike")
            {
                player.DamagePlayer(1);

                //print(player.stats.curHealth);
                StartCoroutine(character.KnockBackSpike(0.02f, 0.8f, character.transform.position));
            }
            if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "EnemyUFO")
            {
                knockBack = true;
                player.DamagePlayer(10);
                character.KnockBackMonster(4f, knockBackFromRight);
                StartCoroutine((Wait()));

            }
        }
        // cho sau mot khoang thoi gian nhat dinh
        public IEnumerator Wait()
        {

            yield return new WaitForSeconds(0.75f);
            knockBack = false;
        }
        public bool GetKnockBack()
        {
            return knockBack;
        }


    }
}