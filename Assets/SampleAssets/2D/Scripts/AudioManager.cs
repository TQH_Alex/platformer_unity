using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class Sound {

	public string name;
	public AudioClip clip;


	[Range(0f, 1f)]
	public float volume = 0.7f;
	[Range(0.5f, 1.5f)]
	public float pitch = 1f;

	[Range(0f, 0.5f)]
	public float randomVolume = 0.1f;
	[Range(0f, 0.5f)]
	public float randomPitch = 0.1f;
    // lap am thanh
    public bool loop = false;

    private AudioSource source;

	public void SetSource (AudioSource _source)
	{
		source = _source;
		source.clip = clip;
        source.loop = loop;
    }

	public void Play () {
		source.volume = volume * (1 + UnityEngine.Random.Range(-randomVolume / 2f, randomVolume / 2f));
		source.pitch = pitch * (1 + UnityEngine.Random.Range(-randomPitch / 2f, randomPitch / 2f));
		source.Play();
	}
    public void Stop()
    {
        if (source != null)
        {
            source.Stop();
        }
    }
    public void Pause()
    {
        source.Pause();
    }
    public void unPause()
    {
        source.UnPause();
    }
}

public class AudioManager : MonoBehaviour {

	public static AudioManager instance;

	[SerializeField]
	Sound[] sounds;
    public bool menu = true;
	void Awake ()
	{
		if (instance != null)
		{
			Debug.LogError("More than one AudioManager in the scene.");
		} else
		{
			instance = this;
		}
	}

	void Start ()
	{
		for (int i = 0; i < sounds.Length; i++)
		{
			GameObject _go = new GameObject("Sound_" + i + "_" + sounds[i].name);
			_go.transform.SetParent(this.transform);
			sounds[i].SetSource (_go.AddComponent<AudioSource>());
		}

        // choi nhac cua man o day
        if(menu)
        {
            PlaySound("Music");
        }
        if (GameMaster.spawnCount == 0)
        {
            print(GameMaster.spawnCount);
            PlaySound("Tut");
            //StopSound("Music");
        }
        
    }
    void FixedUpdate()
    {
    }
    // choi nhac
	public void PlaySound (string _name)
	{
		for (int i = 0; i < sounds.Length; i++)
		{
			if (sounds[i].name == _name)
			{
				sounds[i].Play();
				return;
			}
		}

		// no sound with _name
		Debug.LogWarning("AudioManager: Sound not found in list, " + _name);
	}
    // dung choi nhac
    public void StopSound(string _name)
    {
        
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == _name)
            {
                sounds[i].Stop();
                return;
            }
        }
        Debug.LogWarning("AudioManager: Sound not found in list, " + _name);
    }
    public float fadeTime = 1; // fade time in seconds
    public void FadeSound(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if ( fadeTime == 0)
            {
                sounds[i].volume = 0;
                return;
    
            }
            if (sounds[i].name == _name)
            {
                _FadeSound(sounds[i]);
            }

        }
      
    }
    IEnumerator _FadeSound(Sound sounds)
    {
        float t = fadeTime;
        while (t > 0)
        {
            yield return null;
            t -= Time.deltaTime;
            sounds.volume = t / fadeTime;
        }
        yield break;
    }

    // dung tat ca am thanh
    public void StopAllSound()
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            sounds[i].Stop();
        }
        return;

    }
             
    
    // dung tat ca  nhac
    public void PauseAllSound()
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            sounds[i].Pause();         
        }
        return;
 
    }
    public void UnPauseAllSound()
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            sounds[i].unPause();
        }
        return;

    }
    public bool isPlaying(string _name)
    {
        if (sounds[GameMaster.spawnCount].name == _name)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    

}
