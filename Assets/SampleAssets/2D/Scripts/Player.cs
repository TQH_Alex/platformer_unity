﻿using UnityEngine;
using UnitySampleAssets._2D;
using System.Collections;
[RequireComponent(typeof(Platformer2DUserControl))]

public class Player : MonoBehaviour {
    // lop quan ly cac thuoc tinh cua nhan vat
    // Serializable de hien cac thuoc tinh trong lop con ra
    [System.Serializable]
    // mau se duoc hien thi o script Bar
    public class PlayerStats
    {
        public int maxHealth = 100;
        private int _curHealth;
        public int curHealth
        {
            get { return _curHealth;}
           set { _curHealth = Mathf.Clamp(value, 0, maxHealth); }

        }
        // so diem cua nguoi choi
        public static int playerMaxScore = 999999;
        private static int _playerScore = 0;
        public static int PlayerScore
        {
            get { return _playerScore; }
            set { _playerScore = Mathf.Clamp(value, 0, playerMaxScore); }
        }
        public void Init()
        {
            curHealth = maxHealth;
            //PlayerScore = 0;
        }
        public void setCurrentHealth(int health)
        {
            curHealth = health;
        }
    }
    //cache
    private AudioManager audioManager;
    private Platformer2DUserControl controller;
    public static bool freezingPlayer = false;
    void Start()
    {
        // khoi tao mau cho nguoi choi
        stats.Init();
        controller = GetComponent<Platformer2DUserControl>();
        GameMaster.gm.onTogglePauseMenu += OnPauseMenuToggle;
        GameMaster.gm.onToggleVictoryMenu += OnVictoryMenuToggle;
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("FREAK OUT! No AudioManager found in the scene.");
        }
    }
    // kick thoat delegate, de ngung nguoi choi
    void OnPauseMenuToggle(bool active)
    {
        GetComponent<Platformer2DUserControl>().enabled = !active;
        GetComponent<BulletAnimation>().enabled = !active;
        //GetComponent<PlatformerCharacter2D>().enabled != active;
        if (!freezingPlayer)
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY  | RigidbodyConstraints2D.FreezeRotation;
            freezingPlayer = true;
        }
      else
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
            freezingPlayer = false;
        }
    }
    // delegate cua victory
    void OnVictoryMenuToggle(bool active)
    {
        GetComponent<Platformer2DUserControl>().enabled = !active;
        GetComponent<BulletAnimation>().enabled = !active;
        //GetComponent<PlatformerCharacter2D>().enabled != active;
        if (!freezingPlayer)
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
            freezingPlayer = true;
        }
        else
        {
            GetComponent<Rigidbody2D>().constraints =  RigidbodyConstraints2D.FreezeRotation;
            freezingPlayer = false;
        }
    }




    public PlayerStats stats = new PlayerStats();
    // Gioi han rot
    public int fallBoundary = -20;

    void Update()
    {
        // khoi tao bien nguoi choi
      
        // xet vi tri nguoi choi
        if (transform.position.y <= fallBoundary)
            DamagePlayer(9999999);
    }

    // Damage cho player
    public void DamagePlayer(int damage)
    {
       stats.curHealth -= damage;
        print("Damage" + stats.curHealth);
        if (stats.curHealth <= 0)
        {
            GameMaster.KillPlayer(this);
        }
    }
    // ney nguoi choi chet ta se di nguoc lai ham Delegate o lop GameMaster
    void OnDestroy()
    {
        GameMaster.gm.onTogglePauseMenu -= OnPauseMenuToggle;
        GameMaster.gm.onToggleVictoryMenu -= OnVictoryMenuToggle;
        // khi chet thi dung tien chan cua nguoi may'
        if (audioManager != null)
        {
            audioManager.StopSound("BrainbotWalk");
            BrainbotAI.setBrainBotPlayedSound(false);
        }
    }



}
