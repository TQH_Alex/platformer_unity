﻿using UnityEngine;
using System.Collections;
namespace UnitySampleAssets._2D
{
    public class BulletController : MonoBehaviour {
        public float speed; // toc do vien dan
        Animator anim;
      //  public GameObject playerObject;
        public PlatformerCharacter2D player;
        public float lifeTime; // thoi gian dan ton tai
        public bool hasParameterAni;
        BulletAnimation bulletAnim;

        public GameObject bulletFX;
        public Transform blastRadius;
        bool isInBlast;

        // Use this for initialization
        void Start() {
            player = FindObjectOfType<PlatformerCharacter2D>();
            if(player.transform.localScale.x < 0) // quay qua trai
            {
                speed = -speed;
            }
            anim = GetComponent<Animator>();

            bulletAnim = FindObjectOfType<BulletAnimation>();
            Invoke("CreateMyInstance", lifeTime);
            Destroy(gameObject, lifeTime);
           
        }

        // Update is called once per frame
        void Update() {
            // co nam nam trong vung no khong

            double d = Time.deltaTime;
            float f = (float)d;
            GetComponent<Rigidbody2D>().velocity = new Vector2(speed, GetComponent<Rigidbody2D>().velocity.y);
            // neu co bien trong animator thi bo vao day de kich hoat
            if (hasParameterAni)
            {
               
            }
            // chi xet loai dan thu 2 va cho no quay
            if(bulletAnim.iBulletMode == 1)
            {
                transform.Rotate(0, 0, 120f * f);
            }   
        }
        void OnCollisionEnter2D(Collision2D other)
        {
            // chi xet loai dan thu 2
            if (bulletAnim.iBulletMode == 1)
            {
                Vector3 explosionPos = transform.position;
                // lay vi tri tat ca cac vat
                Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPos, 3f, 1 << LayerMask.NameToLayer("Enemy") );
                Collider2D[] collidersFlying = Physics2D.OverlapCircleAll(explosionPos, 3f, 1 << LayerMask.NameToLayer("Flying Enemy"));

                Instantiate(bulletFX, transform.position, transform.rotation);
                Destroy(gameObject);


                foreach (Collider2D hit in colliders)
                {

                    if (hit.gameObject.tag == "Enemy" || hit.gameObject.tag == "EnemyUFO")
                    {
                        hit.gameObject.GetComponent<Enemy>().DamageEnemy(bulletAnim.damage);
                        //Destroy(hit.gameObject);
                    }
                }
                foreach (Collider2D hit in collidersFlying)
                {

                    if (hit.gameObject.tag == "Enemy" || hit.gameObject.tag == "EnemyUFO")
                    {     
                        hit.gameObject.GetComponent<Enemy>().DamageEnemy(bulletAnim.damage);
                        //Destroy(hit.gameObject);
                    }
                }
            }
            // cac loai dan khac
            else
            {
                if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "EnemyUFO")
                {
                    other.gameObject.GetComponent<Enemy>().DamageEnemy(bulletAnim.damage);
                    //Destroy(other.gameObject);
                }
            }
            Instantiate(bulletFX, transform.position, transform.rotation);
            Destroy(gameObject);
        }
        void CreateMyInstance()
        {
            Instantiate(bulletFX, transform.position, transform.rotation);
        }
    }
}
