﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Bar : MonoBehaviour {

    // Use this for initialization
    //[SerializeField]
    //private float _fillAmount;
    // thanh chu
    [SerializeField]
    private Text healthText;
    // thanh hinh
    [SerializeField]
    private Image content;
    public GameObject  player;
    private float lerpSpeed = 1;

    [SerializeField]
    private Color highColor;
    [SerializeField]
    private Color lowColor;
	void Start () {
        // tim vat co ten
        //player = GameObject.FindGameObjectWithTag("Player");
    }
	
	// Update is called once per frame
	void Update () {
        if (player == null)
        {
            HandelBar(0, 1000,5);
            player = FindPlayer();
            return;
        }
        if (GameObject.FindGameObjectWithTag("Player") == null)
        {
       
            player = FindPlayer();
            return;
              //print(player.stats.curHealth);
        }
      
       HandelBar(player.GetComponent<Player>().stats.curHealth, player.GetComponent<Player>().stats.maxHealth,lerpSpeed);
        

    }
    private void HandelBar (float cur, float Max,float lerpSpeed)
    {
       if(content.fillAmount != cur)
        {
            content.fillAmount = Mathf.Lerp(content.fillAmount, Map(cur, 0, Max, 0, 1), Time.deltaTime * lerpSpeed);
        }
        content.color = Color.Lerp(lowColor, highColor, Map(cur, 0, Max, 0, 1)*2);
        
        SetHealth(cur);
    }
    // value la gia tri mau hien tai
    // inMin gia tri khi chet, o day ta chon 0
    // inMax la gia tri lon nhat cua cay mau, co the la 100, hay 1000 ...
    /*outMax la gia tri lon nhat cua khoang [0-1]ma ta muon chuyen value ve 
     * o day outMax la 1, outMin la 0
    */


    private float Map(float value,float inMin,float inMax, float outMin, float outMax )
    {
        return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }
    public void SetHealth(float _cur)
    {
        healthText.text = "Health :" + _cur;
    }
    GameObject FindPlayer ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        return player;
    }
}
