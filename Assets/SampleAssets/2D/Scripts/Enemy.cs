using UnityEngine;

public class Enemy : MonoBehaviour {

    [System.Serializable]
    public class EnemyStats
    {
        public int maxHealth = 100; // muc mau toi da

        private int _curHealth; // mau hien tai
       
        public int curHealth
        {
            get { return _curHealth; }
            set { _curHealth = Mathf.Clamp(value, 0, maxHealth); }
        }
        public void Init()
        {
            curHealth = maxHealth;
        }
    }

    public EnemyStats stats = new EnemyStats();

    [Header("Optional: ")]
    [SerializeField]
    // Bien giu tinh trang cua ke thu
    private StatusIndicator statusIndicator; 

    void Start()
    {
        stats.Init();

        if (statusIndicator != null)
        {
            statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
        }
        // delegate tu lop GameMaster, ngung tat ca cac hoat dong cua quai vat
       GameMaster.gm.onTogglePauseMenu += OnPauseMenuToggle;
        GameMaster.gm.onToggleVictoryMenu += OnVictoryMenuToggle;
    }

    // tat hoac mo cac thanh phan sau cua menu delegate
    void OnPauseMenuToggle(bool active)
    {
        if (GetComponent<EnemyAI>() != null)
        {
            GetComponent<EnemyAI>().enabled = !active;
        }
        if (GetComponent<GloopController>() != null)
        {
            GetComponent<GloopController>().enabled = !active;
        }
        if (GetComponent<TurretAI>() != null)
        {
            GetComponent<TurretAI>().enabled = !active;
        }

        if (GetComponent<BrainbotAI>() != null)
        {
            GetComponent<BrainbotAI>().enabled = !active;
        }
    }
    //delegate cua victory
    void OnVictoryMenuToggle(bool active)
    {
        if (GetComponent<EnemyAI>() != null)
        {
            GetComponent<EnemyAI>().enabled = !active;
        }
        if (GetComponent<GloopController>() != null)
        {
            GetComponent<GloopController>().enabled = !active;
        }
        if (GetComponent<TurretAI>() != null)
        {
            GetComponent<TurretAI>().enabled = !active;
        }
        if (GetComponent<BrainbotAI>() != null)
        {
            GetComponent<BrainbotAI>().enabled = !active;
        }
    }






    // Damage enemy se duoc thuc hien lop Bullet Controller
    public void DamageEnemy(int damage)
    {
        stats.curHealth -= damage;
        if (stats.curHealth <= 0)
        {
            GameMaster.KillEnemy(this);
            Player.PlayerStats.PlayerScore += 50;
        }

        if (statusIndicator != null)
        {
            statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
        }
    }
   
 
        public  float getHealth()
    {
        return stats.curHealth;
    }
    // khi chet hoac huy loai tru Delegate tu lop GameMaster
    void OnDestroy()
    {
        GameMaster.gm.onTogglePauseMenu -= OnPauseMenuToggle;
        GameMaster.gm.onToggleVictoryMenu -= OnVictoryMenuToggle;
    }
    //void OnDisable()
    //{
    //    GameMaster.gm.onTogglePauseMenu -= OnPauseMenuToggle;
    //}
}
