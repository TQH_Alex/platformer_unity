using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameUIEvent : MonoBehaviour {
    void Start()
    {
    //    GameMaster.gm.onTogglePauseMenu += OnPauseMenuToggle;
    }

    public void Quit ()
	{
		Debug.Log("APPLICATION QUIT!");
		Application.Quit();
	}
    public void Menu()
    {
        
        SceneManager.LoadScene(0);
    }

    public void Retry ()
	{
      // GameMaster.gm.onTogglePauseMenu -= OnPauseMenuToggle;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

	}

    private void OnPauseMenuToggle(bool active)
    {
        return;
    }

    public void Resume()
    {
        GameMaster.keyEscape = true;
        Console.WriteLine("Resume");

    }
   
}
