﻿using UnityEngine;
using System.Collections;

public class SpawnPointController : MonoBehaviour {
    public int spawnPointCount = 0;
    public bool saved = false;
    Animator anim;
    public static bool isPlaying;

    //cache
    private AudioManager audioManager;
    void Awake()
    {
        anim = GetComponent<Animator>();
        if(spawnPointCount == 0)
        {
            saved = true;
            anim.SetBool("Saved",saved);
        }
    }
    void Start()
    {
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("FREAK OUT! No AudioManager found in the scene.");
        }
    }
	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            if(spawnPointCount != GameMaster.spawnCount)
            {
                isPlaying = false;
            }
            print("Come to respawn point");
            GameMaster.spawnCount = spawnPointCount;
            saved = true;
            anim.SetBool("Saved", saved);
            if(spawnPointCount == 1 && !isPlaying)
            {
                audioManager.PlaySound("Music");
                audioManager.StopSound("Tut");
                isPlaying = true;
            }
            if (spawnPointCount == 2 && !isPlaying)
            {
                audioManager.PlaySound("BossBattleTheme");
                audioManager.StopSound("Music");
                audioManager.StopSound("Tut");
                isPlaying = true;
            }
        }
    }
}
