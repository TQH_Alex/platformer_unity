﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

    // gia tri cua dong tien
    public int value;
    //cache
    private AudioManager audioManager;
    Animator anim;
    BoxCollider2D box2d;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        box2d = GetComponent<BoxCollider2D>();
        //caching
        // khoi tao audio manager
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("FREAK OUT! No AudioManager found in the scene.");
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if( other.CompareTag("Player"))
        {
            anim.SetBool("Earn",true);
            box2d.enabled = false;
            Player.PlayerStats.PlayerScore += value;
            audioManager.PlaySound("CollectCoin");
            GetComponent<Rigidbody2D>().velocity = new Vector3(GetComponent<Rigidbody2D>().velocity.x,
                    5);
        }
    }
    void DestroyCoin()
    {
        Destroy(gameObject,0.3f);
    }
}
