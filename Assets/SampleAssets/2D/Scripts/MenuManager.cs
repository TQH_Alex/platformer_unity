using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    // khi chuot di ngang qua
    [SerializeField]
    string hoverOverSound = "ButtonHover";

    // khi nhan chuot
    [SerializeField]
    string pressButtonSound = "ButtonPress";

    AudioManager audioManager;
    // khoi ao am thanh
    void Start()
    {
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("No audiomanager found!");
        }
    }

    public void StartGame ()
	{
        audioManager.PlaySound(pressButtonSound);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

	public void QuitGame()
	{
        audioManager.PlaySound(pressButtonSound);
        Debug.Log("WE QUIT THE GAME!");
		Application.Quit();
	}
    // khi chuot di ngang qua
    public void OnMouseOver()
    {
        audioManager.PlaySound(hoverOverSound);
    }

}
