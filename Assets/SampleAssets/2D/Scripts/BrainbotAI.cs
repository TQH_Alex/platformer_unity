﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]
[RequireComponent(typeof(TurretAI))]
[RequireComponent(typeof(GloopController))]
public class BrainbotAI : MonoBehaviour {

    public string walkSoundName = "BrainbotWalk"; 
    //cache
    private AudioManager audioManager;
    public Animator anim;
    //bool
    bool awake;
    public static bool played = false;
    bool attack;
    TurretAI aiTurretPart;
    GloopController aiGloopPart;
    Enemy enemy;
    //ref
    public GameObject bullet;
    public Transform MainfirePoint;
    public GameObject EnemyList;
    public GameObject blockWall;
    public GameObject SupportTurret;
    public GameObject ExplosionDeath;
    // float
    public float timer;
    public float shootInteval;
    public float bulletSpeed = 100;
    public float walkSpeed = 2;
    //
    public GameObject target;
    // Use this for initialization
    void Awake()
    {
        played = false;
    }
    void Start () {
        //caching
        // khoi tao audio manager
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("FREAK OUT! No AudioManager found in the scene.");
        }
        aiTurretPart = GetComponent<TurretAI>();
        aiGloopPart = GetComponent<GloopController>();
        anim = gameObject.GetComponent<Animator>();
        enemy = GetComponents<Enemy>()[0];
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }
        // chi doi huong di khi nguoi choi vao cung tan cong
        if (aiTurretPart.GetAwake() && (aiTurretPart.GetTurretTooClose() != true))
        {
            //anim.SetBool("Attack", false);

            // di qua trai, nhin ve phai thi doi huong di
            if (aiGloopPart.getMoveLeft() == false && aiTurretPart.GetTurretLookingRight() == true)
            {
                aiGloopPart.SetMoveDirection();
            }
            // di qua phai, nhin ve trai thi doi huong di
            if ((aiGloopPart.getMoveLeft() == true) && aiTurretPart.GetTurretLookingRight() == false)
            {
                aiGloopPart.SetMoveDirection();
            }
        }
        //print(aiTurretPart.GetDistance());
        //print(aiTurretPart.GetWakeUpRange());
        if (aiTurretPart.GetDistance() < aiTurretPart.GetWakeUpRange() && !anim.GetBool("Attack"))
        {
            if (!played)
            {
                audioManager.PlaySound(walkSoundName);
                played = true;
            }
        }
        if (aiTurretPart.GetDistance() > aiTurretPart.GetWakeUpRange())
        {

            if (played)
            {
                audioManager.StopSound(walkSoundName);
                played = false;
            }
        }
        // neu mau thap hon 500 thi tang gap 3 lan toc do
        //print(enemy.getHealth());
        if (enemy.getHealth() < 1000)
        {
            aiGloopPart.SetMoveSpeed(3);
        }
        if (enemy.getHealth() < 500)
        {
            aiGloopPart.SetMoveSpeed(6);
            SupportTurret.SetActive(true);
        }

    }
    // BrainBot tan cong
    public void BrainBotAttack()
    {
      
        // print("Attack");
        timer += Time.deltaTime;
        if (timer >= shootInteval)
        {
            // tao huong ban dan
            Vector2 direction = target.transform.position - transform.position;
            direction.Normalize();
            // ban tu ben trai
            GameObject bulletClone;
            bulletClone = Instantiate(bullet, MainfirePoint.position, MainfirePoint.rotation) as GameObject;
            bulletClone.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

            timer = 0;

        }
        
    }
    public static void setBrainBotPlayedSound(bool _played)
    {
        played = _played;
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            BrainBotAttack();
            
            //anim.SetBool("Attack", true);
            //walkSpeed = 0;
            //anim.SetFloat("Speed", walkSpeed);
            //GetComponent<GloopController>().enabled = false;
        }
    }
    // luc boss bi tieu diet
    void OnDestroy()
    {
        audioManager.StopSound(walkSoundName);
        audioManager.StopSound("BossBattleTheme");
        audioManager.PlaySound("Tut");
        audioManager.PlaySound("BossExplosion");
        Instantiate(ExplosionDeath, transform.position, transform.rotation);
        EnemyList.SetActive(false);
        blockWall.SetActive(false);
        SupportTurret.SetActive(false);
    }
   
    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.CompareTag("Player") && aiTurretPart.GetTurretTooFar() == true)
        {
            // BrainBotAttack();       
            //audioManager.StopSound(walkSoundName);
            //played = false;       
          
        }
    }
   
        // tang Awake Range o lop TurretAi de tang do xa cua am thanh
        void OnTriggerExit2D (Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            
        }
    }
    void OnCollisionEnter2D(Collision2D other)
    {
      
    }
   
    void PlayWalkSound()
    {

    }
    void StopWalkSound()
    {
        //audioManager.StopSound(walkSoundName);
    }
    void OnCollsionEnter2D(Collision2D other)
    {
      
    }
}
