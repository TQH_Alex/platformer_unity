﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{
    // lop quan ly vu khi
    // Use this for initialization
    public float fireRate = 0; // toc do ban
    public float Damage = 10; // damage tan cong cua vu khi
    public LayerMask whatToHit; // lop ma vu khi khong the ban qua

    float timeToFire = 0;
    Transform firePoint; // 
    void Start()
    {
        firePoint = transform.FindChild("FirePoint");
        if (firePoint == null)
        {
            Debug.LogError("Khong tim thay FirePoint"); // check loi neu fire point khong ton tai
        }
    }

    // Update is called once per frame
    void Update()
    {
        // kiem tra vu khi ban theo tung vien
        if (fireRate == 0)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Shoot();
            }
        }
        // kiem tra vu khi ban theo loat
        else
        {
            if (Input.GetButton("Fire1") && Time.time > timeToFire)
            {
                timeToFire = Time.time + 1 / fireRate;
                Shoot();
            }
        }
    }
    //ban
    void Shoot()
    {
        Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        Vector2 firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
        RaycastHit2D hit = Physics2D.Raycast(firePointPosition, mousePosition - firePointPosition, 100, whatToHit);
        Debug.DrawLine(firePointPosition, (mousePosition - firePointPosition) * 100, Color.cyan);
        if (hit.collider != null)
        {
            Debug.DrawLine(firePointPosition, hit.point, Color.red);
            Debug.Log("Chung ta da " + hit.collider.name + " va tan cong duoc " + Damage + " damage.");
        }
    }
}
