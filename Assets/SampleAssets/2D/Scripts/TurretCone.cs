﻿using UnityEngine;

public class TurretCone : MonoBehaviour {
    TurretAI turretAI;
    public bool isLeft;

	// Use this for initialization
	void Start () {
        turretAI = gameObject.GetComponentInParent<TurretAI>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerStay2D(Collider2D other)
    {

        if(other.CompareTag("Player"))
        {
            // tan cong ben trai
            if(isLeft && turretAI.GetAwake())
            {
                turretAI.Attack(false);
            }
            // tan cong ben phai
            if (!isLeft && turretAI.GetAwake())
            {
                turretAI.Attack(true);
            }
        }
    }

}
