﻿using UnityEngine;
using System.Collections;
namespace UnitySampleAssets._2D
{
    [RequireComponent(typeof(PlatformerCharacter2D))]
    public class BulletAnimation : MonoBehaviour
    {

        public Transform firePoint; // diem  dan phat sang
        public Transform firePointBullet; // diem dan ban ra
        public GameObject[] bulletType;
        public string mainGunSound = "mainGun";
        public string secondGunSound = "secondGun";

        // Damage cua tung float dan
        public int damage;

        // bool allowfire;
        float timeToFire = 0;
        public float fireRate1; // toc do ban
        GameObject bulletTravel;
        // Use this for initialization
        Animator anima;
        private PlatformerCharacter2D player;

        // hieu ung khi ban dan
        public Transform MuzzleFlashPrefab;

        public int iBulletMode = 0;

        //cache
        private AudioManager audioManager;
        void Start()
        {
            player = FindObjectOfType<PlatformerCharacter2D>();
            audioManager = AudioManager.instance;
            if (audioManager == null)
            {
                Debug.LogError("FREAK OUT! No AudioManager found in the scene.");
            }


        }

        private void Awake()
        {
            anima = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
        
            // Chung cho ca Android va Window

            // giam toc do ban neu la loai dan 1
            if(iBulletMode == 1)
            {
                fireRate1 = 0.5f;
                damage = 100;
        
                
            }
            else
            {
                fireRate1 = 3.5f;
                damage = 50;
            }



#if UNITY_STANDALONE_WIN
                // Doi dang dan
            if (Input.GetKeyDown(KeyCode.R))
            {
               //print("le" + bulletType.Length);
                iBulletMode++;
               // print("i " + iBulletMode);
                if (iBulletMode == bulletType.Length)
                {
                    iBulletMode = 0;
                }

                // print(iBulletMode);

            }
            if (fireRate1 == 0)
            {
              
                if (Input.GetButton("Fire1"))
                {
                   
                    Fire(iBulletMode);
                    // hieu ung cho dan 1
                    if(iBulletMode == 0)
                    {
                        //Effect();
                    }
                }
            }
            else
            {
                if (Input.GetButton("Fire1") && Time.time > timeToFire)
                {
                    if (iBulletMode == 0)
                    {
                        audioManager.PlaySound(mainGunSound);
                    }
                    if (iBulletMode == 1)
                        audioManager.PlaySound(secondGunSound);

                    // neu nhan R timeToFire tre ve lai 0
                    if (Input.GetKeyDown(KeyCode.R))
                    {
                        timeToFire = 0;
                    }
                    else
                    {
                        timeToFire = Time.time + 1 / fireRate1;
                    }
                    Fire(iBulletMode);
                    // hieu ung cho dan 1
                    if (iBulletMode == 0)
                    {
                       // Effect();
                    }
                }
            }
#endif

#if UNITY_ANDROID
            if (GameMaster.changeWeapon)
            {
                //print("le" + bulletType.Length);
                iBulletMode++;
                // print("i " + iBulletMode);
                if (iBulletMode == bulletType.Length)
                {
                    iBulletMode = 0;
                }

                // print(iBulletMode);
                GameMaster.changeWeapon = false;
            }
            if (fireRate1 == 0)
            {

                if (GameMaster.fireAndroid)
                {

                    Fire(iBulletMode);
                    //GameMaster.fireAndroid = false;
                    // hieu ung cho dan 1
                    if (iBulletMode == 0)
                    {
                        //Effect();
                    }
                }
            }
            else
            {
                if (GameMaster.fireAndroid && Time.time > timeToFire)
                {
                    if (iBulletMode == 0)
                    {
                        audioManager.PlaySound(mainGunSound);
                    }
                    if (iBulletMode == 1)
                        audioManager.PlaySound(secondGunSound);

                    // neu nhan R timeToFire tre ve lai 0
                    if (Input.GetKeyDown(KeyCode.R))
                    {
                        timeToFire = 0;
                    }
                    else
                    {
                        timeToFire = Time.time + 1 / fireRate1;
                    }
                    Fire(iBulletMode);
                  //  GameMaster.fireAndroid = false;
                    // hieu ung cho dan 1
                    if (iBulletMode == 0)
                    {
                        // Effect();
                    }
                }
            }
#endif
        }
        void Fire(int iBulletMode)
        {
            
          
            // allowfire = true;
            if (player.transform.localScale.x < 0) // quay qua trai
            {
                Instantiate(bulletType[iBulletMode], firePointBullet.position, Quaternion.Euler(0, -180, 0)); // xoay vien dan qua trai
                                                                                                        // anima.Play("Bullet_travel")
            }
            else
            {
                Instantiate(bulletType[iBulletMode], firePointBullet.position, firePointBullet.rotation); // ban dan o huong cu
              

            }

        }
        // hieu ung khi ban dan
        void Effect()
        {
            Transform clone = Instantiate(MuzzleFlashPrefab, firePoint.position, firePoint.rotation) as Transform;
            clone.parent = firePoint;
            float size = Random.Range(0.8f, 1f);
            clone.localScale = new Vector3(size, size, size);
            Destroy(clone.gameObject, 0.09f);
        }
    }

}

