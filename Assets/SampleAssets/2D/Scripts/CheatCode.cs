﻿using UnityEngine;
using System.Collections;

public class CheatCode : MonoBehaviour {

    private string[] cheatCode;
    private int index;
    private GameObject player;
    private CheatCode cheat;
    private  bool isCheating = false;
    private bool turnOFf = true;
    void Awake ()
    {
        isCheating = false;
        //cheat = this;
    }
    void Start()
    {
        // Code is "idkfa", user needs to input this in the right order
        cheatCode = new string[] { "b", "a", "t", "t", "u" };
        index = 0;
        if (player == null)
        {
            //HandelBar(0, 1000, 5);
            player = FindPlayer();
            return;
        }
        if (GameObject.FindGameObjectWithTag("Player") == null)
        {

            player = FindPlayer();
            return;
            //print(player.stats.curHealth);
        }
    }

    // Update is called once per frame
    void Update () {
        if (isCheating && Input.anyKeyDown && player == null)
        {
            player = FindPlayer();
        }
        // Check if any key is pressed





        // If index reaches the length of the cheatCode string, 
        // the entire code was correctly entered
        if (Input.anyKeyDown)
        {
           turnOnOffCheat();


            if (player != null && !turnOFf)
            {
                // print("CHEAT Success");
                player.GetComponent<Player>().stats.setCurrentHealth(9999);
                isCheating = true;
                //turnOFf = true;
            }
        }
      

    }
    public void turnOnOffCheat()
    {
        if (Input.anyKeyDown && !isCheating)
        {
         
            // Check if the next key in the code is pressed
            if (Input.GetKeyDown(cheatCode[index]))
            {
                // Add 1 to index to check the next key in the code
                index++;
            }
            // Wrong key entered, we reset code typing
            else
            {
                index = 0;
            }
            if (index == cheatCode.Length && !isCheating)
            {
                print("Turn on");
                print("bool turn off " + turnOFf);
                index = 0;
                turnOFf = false;
                return;
            }

        }
     
        if (Input.anyKeyDown && isCheating)
        {
            // Check if the next key in the code is pressed
            if (Input.GetKeyDown(cheatCode[index]))
            {
                // Add 1 to index to check the next key in the code
                index++;
            }
            // Wrong key entered, we reset code typing
            else
            {
                index = 0;
            }
            if (index == cheatCode.Length && isCheating)
            {
                isCheating = false;
                print("Turn off");
                index = 0;
                turnOFf = true;
                return;
            }
        }
    }
  
    //public static bool CheatingCheck ()
    //{
    //    return isCheating;
    //}
    GameObject FindPlayer()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        return player;
    }
}
