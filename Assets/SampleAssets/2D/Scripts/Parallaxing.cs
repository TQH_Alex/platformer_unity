﻿using UnityEngine;
using System.Collections;


 

public class Parallaxing : MonoBehaviour {

    // lop parallax cho background va foreground

    public Transform[] backgrounds; // chua tat ca cac vat duoc xem la background va foreground
    private float[] parallaxScale ; // muc do di chuyen cua camera so voi background
    public float smoothing = 1f;  // do muot cua parallax, phai tren 0


    private Transform cam; // bien chi toi camera
    private Vector3 preCameraPos; // vi tri truoc do cua camara

    // awake duoc goi truoc start, se goi tat ca logic cua game truoc ham start, sau do moi toi khoi tao do vat trong game 
    // tot cho viec gan bien, tham chieu
    void Awake()
    {
        // tham chieu
        cam = Camera.main.transform;
    }
    // Use this for initialization
    void Start () {
        // gan vi tri truoc cua camera cho vi tri hien tai
        preCameraPos = cam.position;
        parallaxScale = new float[backgrounds.Length];
        // gan parallax hop ly voi background
        for (int i = 0; i < backgrounds.Length; i++)
        {
            parallaxScale[i] = backgrounds[i].position.z * -1;
        }
	}
	
	// Update is called once per frame
	void Update () {
        // cho moi background
        for (int i = 0; i < backgrounds.Length; i++)
        {
            //parallax se chay nguoc voi camera, tao hieu ung nen
            float parallax = (preCameraPos.x - cam.position.x) * parallaxScale[i];

            // tao doi tuong , ma vi tri x cua no la diem hien tai + parallax
            float backgroundTargetPosX = backgrounds[i].position.x + parallax;

            // su dung vi tri x cua doi tuong cho vector 3
            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, backgrounds[i].position.y,
                backgrounds[i].position.z);

            // di chuyen giua background va target
            backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
        }
            // gan lai previous camera
            preCameraPos = cam.position;

        
	
	}
}
