﻿using UnityEngine;
using System.Collections;
using Pathfinding;
using System;

[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent (typeof (Seeker))]
public class EnemyAI : MonoBehaviour {

	// Duoi theo thu gi
	public Transform target;
	
	// bao lau thi cap nhap lai thuat toan 
	public float updateRate = 2f;
    float nextTimeToSearch = 0;

    // Luu du lieu
    private Seeker seeker;
	private Rigidbody2D rb;
	
	//Tinh duong di
	public Path path;
	
	//The AI's speed per second
	public float speed = 300f;
	public ForceMode2D fMode;

    //tim kiem nguoi choi
    bool searchingForPlayer = false;
	
	[HideInInspector]
	public bool pathIsEnded = false;
	
	// The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 3;
	
	// The waypoint we are currently moving towards
	private int currentWaypoint = 0;
	
	void Start () {
		seeker = GetComponent<Seeker>();
		rb = GetComponent<Rigidbody2D>();
		
        // neu nguoi choi  chet se bat dau tim kiem
		if (target == null) {

            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchingForPlayer());
            }

			return;
		}
		
		// Start a new path to the target position, return the result to the OnPathComplete method
		seeker.StartPath (transform.position, target.position, OnPathComplete);
		
		StartCoroutine (UpdatePath ());
	}
    // ham tim kiem nguoi choi,
    private IEnumerator SearchingForPlayer()
    {
        GameObject sResult = GameObject.FindGameObjectWithTag("Player");
        if(sResult == null)
        {
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(SearchingForPlayer());
        }
        else
        {
            target = sResult.transform;
            searchingForPlayer = false;
            StartCoroutine(UpdatePath());
        }
    }

    IEnumerator UpdatePath () {
        if (target == null)
        {

            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchingForPlayer());
            }

            yield return true;
        }
        else
        {

            // Start a new path to the target position, return the result to the OnPathComplete method
            seeker.StartPath(transform.position, target.position, OnPathComplete);
        }
		yield return new WaitForSeconds ( 1f/updateRate );
		StartCoroutine (UpdatePath());
	}
	
	public void OnPathComplete (Path p) {
		//Debug.Log ("We got a path. Did it have an error? " + p.error);
		if (!p.error) {
			path = p;
			currentWaypoint = 0;
		}
	}
	
	void FixedUpdate () {
        if (target == null)
        {

            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchingForPlayer());
            }

            return;
        }


        //TODO: Always look at player?

        if (path == null)
			return;
		
		if (currentWaypoint >= path.vectorPath.Count) {
			if (pathIsEnded)
				return;
			
			Debug.Log ("End of path reached.");
			pathIsEnded = true;
			return;
		}
		pathIsEnded = false;
	
		//Direction to the next waypoint
		Vector3 dir = ( path.vectorPath[currentWaypoint] - transform.position ).normalized;
		dir *= speed * Time.fixedDeltaTime;
		
		//Move the AI
		rb.AddForce (dir, fMode);
		
		float dist = Vector3.Distance (transform.position, path.vectorPath[currentWaypoint]);
		if (dist < nextWaypointDistance) {
			currentWaypoint++;
			return;
		}
	}
    // tim nguoi choi cho UFO
     GameObject FindPlayer()
    {
        GameObject searchResult= null;
        if (nextTimeToSearch <= Time.time)
        {
            searchResult = GameObject.FindGameObjectWithTag("Player");
            if (searchResult != null)
                target = searchResult.transform;
            nextTimeToSearch = Time.time + 0.5f;
            return searchResult;
        }

        return searchResult;
    }

}
